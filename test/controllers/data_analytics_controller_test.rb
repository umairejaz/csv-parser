require 'test_helper'

class DataAnalyticsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @data_analytic = data_analytics(:one)
  end

  test "should get index" do
    get data_analytics_url
    assert_response :success
  end

  test "should get new" do
    get new_data_analytic_url
    assert_response :success
  end

  test "should create data_analytic" do
    assert_difference('DataAnalytic.count') do
      post data_analytics_url, params: { data_analytic: { count: @data_analytic.count, first_name: @data_analytic.first_name } }
    end

    assert_redirected_to data_analytic_url(DataAnalytic.last)
  end

  test "should show data_analytic" do
    get data_analytic_url(@data_analytic)
    assert_response :success
  end

  test "should get edit" do
    get edit_data_analytic_url(@data_analytic)
    assert_response :success
  end

  test "should update data_analytic" do
    patch data_analytic_url(@data_analytic), params: { data_analytic: { count: @data_analytic.count, first_name: @data_analytic.first_name } }
    assert_redirected_to data_analytic_url(@data_analytic)
  end

  test "should destroy data_analytic" do
    assert_difference('DataAnalytic.count', -1) do
      delete data_analytic_url(@data_analytic)
    end

    assert_redirected_to data_analytics_url
  end
end
