require 'csv'
require 'net/http/post/multipart'

class DataAnalyticsController < ApplicationController
  before_action :set_data_analytic, only: [:show, :edit, :update, :destroy]

  # GET /data_analytics
  # GET /data_analytics.json
  def index
    @data_analytics = DataAnalytic.all
  end

  # GET /data_analytics/1
  # GET /data_analytics/1.json
  def show
  end

  # GET /data_analytics/new
  def new
    @data_analytic = DataAnalytic.new
  end

  # GET /data_analytics/1/edit
  def edit
  end

  # POST /data_analytics
  # POST /data_analytics.json
  def create

    api_response = get_parsed_data(data_analytic_params[:file])

    api_response.each do |key,val|
      DataAnalytic.create(first_name: key,count: val)
    end

    respond_to do |format|
        format.html { redirect_to data_analytics_url, notice: 'Data analytic was successfully imported.' }
        format.json { render :show, status: :created }
    end
  end

  # PATCH/PUT /data_analytics/1
  # PATCH/PUT /data_analytics/1.json
  def update
    respond_to do |format|
      if @data_analytic.update(data_analytic_params)
        format.html { redirect_to @data_analytic, notice: 'Data analytic was successfully updated.' }
        format.json { render :show, status: :ok, location: @data_analytic }
      else
        format.html { render :edit }
        format.json { render json: @data_analytic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data_analytics/1
  # DELETE /data_analytics/1.json
  def destroy
    @data_analytic.destroy
    respond_to do |format|
      format.html { redirect_to data_analytics_url, notice: 'Data analytic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def get_parsed_data(file)

      url = URI.parse(API_URL)
      response = nil
      File.open(file.path) do |csv|
        req = Net::HTTP::Post::Multipart.new url.path,
                                             "csv" => UploadIO.new(csv, "text/csv", "test1.csv")
        response = Net::HTTP.start(url.host, url.port) do |http|
          http.request(req)
        end

      end
      JSON.parse(response.body)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_data_analytic
      @data_analytic = DataAnalytic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def data_analytic_params
      params.permit(:file)
    end
end
