json.extract! data_analytic, :id, :first_name, :count, :created_at, :updated_at
json.url data_analytic_url(data_analytic, format: :json)
