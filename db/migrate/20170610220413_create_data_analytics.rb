class CreateDataAnalytics < ActiveRecord::Migration[5.1]
  def change
    create_table :data_analytics do |t|
      t.string :first_name
      t.integer :count

      t.timestamps
    end
  end
end
